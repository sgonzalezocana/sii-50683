#include <stdio.h>
#include <string.h>
#include "../include/DatosMemCompartida.h"
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>      
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

float pos_centro1;
	float pos_centro2;

int CalcularPos(float c1, float centroesf){
		
		if(c1<centroesf)
		{
			return 1;
		}
		else if(c1>centroesf)
		{
			return -1;
		}
		else{
			return 0;
		}
		
}


int main(int argc,char* argv[]){

	DatosMemCompartida *dato;
	char *X;
	int fd = open("/tmp/datoscompartidos.txt", O_RDWR, 0777);

	X=(char*)mmap(0, sizeof(*(dato)), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	
	close(fd);
	
	dato = (DatosMemCompartida*)X;
	
	while(1){
		pos_centro1 = (dato->raqueta1.y1, dato->raqueta1.y2)/2;
		pos_centro2 = (dato->raqueta2.y1, dato->raqueta2.y2)/2;
		
		dato->accion1=(CalcularPos(pos_centro1, dato->esfera.centro.y));
		dato->accion2=(CalcularPos(pos_centro2, dato->esfera.centro.y));

		usleep(25000);
		
	}
	munmap(X, sizeof(*(dato)));
	return 0;
}
